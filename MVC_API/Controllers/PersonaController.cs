﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MVC_API.Models;

namespace MVC_API.Controllers
{
    public class PersonaController : ApiController
    {
        // GET: api/Persona
        public IHttpActionResult Get()
        {
            midb1 midb = new midb1();
            List<Persona> Personas = midb.Personas.ToList();
            return Ok(Personas);
        }

        // GET: api/Persona/5
        public IHttpActionResult Get(int id)
        {
            midb1 midb = new midb1();
            Persona  persona = midb.Personas.Find(id);
            return Ok(persona);
        }

        // POST: api/Persona
        public IHttpActionResult Post([FromBody] Persona personaInsert)
        {
            midb1 midb = new midb1();
            midb.Personas.Add(personaInsert);
            midb.SaveChanges();
            return Ok();
        }

        // PUT: api/Persona/5
        public IHttpActionResult Put(int id, [FromBody] Persona personaUpdate)
        {
            midb1 midb = new midb1();
            Persona persona = midb.Personas.Find(id);
            if (persona == null)
            {
                return NotFound();
            }
            persona.Apellido = personaUpdate.Apellido;
            persona.Nombre = personaUpdate.Nombre;
            persona.Telefono = personaUpdate.Telefono;
            midb.SaveChanges();
            return Ok();
        }

        // DELETE: api/Persona/5
        public IHttpActionResult Delete(int id)
        {
            midb1 midb = new midb1();
            Persona personaDelete = midb.Personas.Find(id);
            if (personaDelete == null)
            {
                return NotFound();
            }
            midb.Personas.Remove(personaDelete);
            midb.SaveChanges();
            return Ok();
            
        }
    }
}
